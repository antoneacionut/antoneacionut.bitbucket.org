// =menu
  $(document).ready(function () {
    $("#menu>ul>li").on("mouseenter", function(){	 
     $(this).find(">:nth-child(2)").css({"display":"none"}).slideDown(); 
	}).on("mouseleave", function(){	 
     $(this).find(">:nth-child(2)").css({"display":"none"}); 
	})
  })
//end menu


// =slider
$(document).ready(function(){
  
  $("#photosSlider").css({"width":"382px","height":"282px","overflow":"hidden"}); //modifing css values that were set for javascript off
  var totalImgs = $("#photosSlider").children().length;							//total number of images
  
  
  for (i=1;i<=totalImgs;i++){/*arranging the slide images, not depending on their total number*/
	 var dist=(i-1)*15; 
	 $("#photosSlider img:nth-child("+i+")").css({"position":"absolute","left": dist,"bottom": dist,"z-index": -i})
  }
  
  
  for (i=2;i<=totalImgs;i++){/*setting the border for all images, excepting the first one*/
     $("#photosSlider img:nth-child("+i+")").css({"border": "5px solid #f5f6f6","opacity": .8})
  }
  
  
  $("#controls ul li").on("click", function () {/*handling the click event on the controls*/
    var clicked = $("#controls ul li").index(this)+1;//the index of the control that was clicked coresponding to the picture
	
	//checking which img has the highest index
	var zindexArray = [];
    $("#photosSlider img").each(function(){
	   zindexArray.push($(this).css("z-index"));
	})
	var highestZindex = Math.max.apply(Math, zindexArray) //highest z-index
	var newZindex = highestZindex +1;                     //the new z-index for the picture that was called (highest index + 1) 
	
    //the picture that was called will get its position in front, no border, opacity 1 and the highest new z index
	$("#photosSlider img:nth-child("+clicked+")")
	  .animate({
	   "left": 0,
	   "bottom": 0
	   },1000)
	  .css({"border":"none","z-index": newZindex, "opacity": 1})

	//top and left integer values for the image that was called
    var leftClicked = parseInt($("#photosSlider img:nth-child("+clicked+")").css("left"),10);
	var bottomClicked = parseInt($("#photosSlider img:nth-child("+clicked+")").css("bottom"),10);
    
	//all images with lower top and left will increase with 15px
	$("#photosSlider img").each(function(){
	  var a = parseInt($(this).css("left"),10);
	  if (a<leftClicked ){
	     $(this).animate({
		  	"left": "+=15",
	   		"bottom": "+=15"
		 },1000).css({"border": "5px solid #f5f6f6","opacity": .8})
	  }
	})

  })
})//end of ready function
//end slider


//the gallery

$(document).ready(function(){
  
  //remaking the css style
  $(".fields").css({"overflow":"hidden"});
  $(".fields").siblings().on("mouseover", function() {
    $(this).css({"background-color":"#CFCFCF"})
  }).on("mouseleave", function() {
    $(this).css({"background-color":"#FAFAFA"})
  })
  $(".gallery .cell").css({"display":"block"});
  $(".gallery").css({"padding-left":"30px"});
  //
  
  var totalImgs = $(".fields ul").children().length,
      i=0;
  
  
  //left and right click events
  $(".leftControl img").on("click",function(){
	if (i==totalImgs) {
	  i=1;
	  var marginLeft = -(i % totalImgs)*640;  
      $(".fields ul").animate({"margin-left": marginLeft},400);
	}
	else{
	 i++;
	 var marginLeft = -(i % totalImgs)*640;  
     $(".fields ul").animate({"margin-left": marginLeft},400);
	}
	console.log(i);
  });
  
  $(".rightControl img").on("click",function(){
	if (i==0){
	 i=(totalImgs-1)
	 var marginLeft = -(i % totalImgs)*640;  
     $(".fields ul").animate({"margin-left": marginLeft},400)	 
	}
    else {
	 i--;
	 var marginLeft = -(i % totalImgs)*640;  
     $(".fields ul").animate({"margin-left": marginLeft},400);
	}
	console.log(i);
  });

})



//end gallery